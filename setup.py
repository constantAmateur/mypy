#!/usr/bin/env python

import os
import sys
import distutils
from distutils.core import setup

_top_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0,os.path.join(_top_dir, "lib"))

try:
    import mypy
finally:
    del sys.path[0]

setup(
    name="mypy",
    version=mypy.__version__,
    author="Matthew Young",
    author_email="my304@ast.cam.ac.uk",
    url="https://bitbucket.org/constantAmateur/mypy",
    download_url="https://bitbucket.org/constantAmateur/mypy",
    license="LICENSE.txt",
    platforms=["any"],
    packages=['mypy'],
    install_requires =
    ['matplotlib','scipy','numpy','progressbar','pylab','struct'],
    description="Collection of routines that are helpful.",
    long_description=open("README.md").read(),
)
