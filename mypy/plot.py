import matplotlib as mpl
import numpy as np
from matplotlib import pyplot as plt
from scipy.interpolate import griddata,interp1d
from .math import sround

def trueBoxplot(data,tgt=None,col='blue',**kw):
  """Creates a boxplot according to the more useful convention of 5%-95% whiskers.  That is
  the whiskers, box and median mark the 5,25,50,75,95 percentiles of the data.  Currently only
  supports drawing one boxplot at a time, so data must be a 1d array.  Col sets the colour of
  the outliers.  This is broken in boxplot."""
  if tgt is None:
    tgt=plt
  qs=np.percentile(data,[5,25,50,75,95])
  #This only gets the box part right, so the rest doesn't matter
  bp=tgt.boxplot(data,**kw)
  #Identify each of the elements
  #The arms
  arm5=bp['whiskers'][0]
  if np.any(arm5.get_data()[1]==qs[1]):
    arm95=bp['whiskers'][1]
  else:
    arm5=bp['whiskers'][1]
    arm95=bp['whiskers'][0]
  #The caps
  cap5=bp['caps'][0]
  cap95=bp['caps'][1]
  if not (np.all(cap5.get_data()[1]==qs[0]) or np.all(cap95.get_data()[1]==qs[4])):
    cap95=bp['caps'][0]
    cap5=bp['caps'][1]
  #The outliers
  fly5=bp['fliers'][0]
  if np.all(fly5.get_data()[1]<=qs[1]):
    fly95=bp['fliers'][1]
  else:
    fly5=bp['fliers'][1]
    fly95=bp['fliers'][0]
  #Change the plot
  arm5.set_data((arm5.get_data()[0],np.array([qs[0],qs[1]])))
  cap5.set_data((cap5.get_data()[0],np.array([qs[0],qs[0]])))
  tmp=fly5.get_data()
  o=np.where(tmp[1]<qs[0])[0]
  fly5.set_data(tmp[0][o],tmp[1][o])
  arm95.set_data((arm95.get_data()[0],np.array([qs[3],qs[4]])))
  cap95.set_data((cap95.get_data()[0],np.array([qs[4],qs[4]])))
  tmp=fly95.get_data()
  o=np.where(tmp[1]>qs[4])[0]
  fly95.set_data(tmp[0][o],tmp[1][o])
  #Set the colour of outliers
  fly5.set_color(col)
  fly95.set_color(col)
  return bp

def plot(x, y=None, hline=None, vline=None, box=False, xlab='', ylab='',
      title='', log='', pch='o', add=False, fig=None,
      subplot=111,cmap=mpl.cm.RdYlGn_r,colbar=False,boxSym='',**kw):
  """
  General purpose plotting function. 

  If y=None, y=range(len(x))

  hline and vline are arrays (or single numbers) specifying where to draw
  horizontal/vertical red lines.

  box will draw a boxplot on the left of the figure.

  xlab,ylab,title and the labels for the axis and the title.

  pch sets the character type.  Can be either a valid character (see help for
  scatter) or a number which will be converted.
  
  The add parameter can be set to an axis instance, in which case the call is made to
  that particular axes instance.  If just add==True then the currently active plot is
  used.  Either way fig and subplot are ignored.

  If fig is set, it is that instance of Figure that the plotting is done to.  Otherwise
  a new figure object is made with a call to plt.Figure().

  If not add, then a new instance of subplot is added to either the newly made 
  figure object, or the one passed by the user.  subplot is the parameter that it
  is called with.

  This function calls scatter, so any valid keywords for scatter will be
  honoured.  Of particular use are the c (alias col), s (size)
  arguments which control the colour of points, there size and can be either a
  single value, or an array of length x.

  cmap defines the mapping between numbers and colours used by c.  By default
  small numbers give green, large given red (yellow intermediate).

  if colbar=True, then a colorbar will be added using the data supplied to c.
  Obviously c must be given for this to work...

  boxSym is the symbol used for outliers in the boxplot
  
  Returns [Figure (can be none if add==True),boxplot (if any),plot,colorbar (if
  any)]

  CURRENTLY INACTIVE.
  log
  multi-character pch
  """
  if y is None:
    y=np.arange(len(x))
  #What is the target axes object we'll plot everything to?
  if add:
    #If we're adding something, we don't HAVE to have the fig object...
    if isinstance(add,plt.Axes):
      tgt=add
    else:
      tgt=plt
  elif fig:
    tgt=fig.add_subplot(subplot)
  else:
    fig=plt.figure()
    tgt=fig.add_subplot(subplot)
  ret=[fig]
  pchConvert = ['o','s','^','>','v','<','d','p','h','8','+','x']
  if not isinstance(pch,str):
    pch=pchConvert[pch%len(pchConvert)]
  pltfunc=tgt.scatter
  if log.find('x')>=0 and log.find('y')>=0:
    #pltfunc=tgt.loglog
    pass
  elif log.find('x')>=0:
    #pltfunc=tgt.semilogx
    pass
  elif log.find('y')>=0:
    #pltfunc=tgt.semilogy
    pass
  if box and not add:
    qs=np.percentile(y,[5,25,50,75,95])
    whis=np.max((qs[4]-qs[2],qs[2]-qs[0]))/(qs[3]-qs[1])
    xsize=sround((x.max()-x.min())/10.)
    #boxobj=tgt.boxplot(y,positions=[x.min()-xsize],widths=[xsize],whis=whis)
    boxobj=trueBoxplot(y,positions=[x.min()-xsize],widths=[xsize],tgt=tgt,sym=boxSym)
    ret.append(boxobj)
  #make col a synonym for color to have R like syntax
  if 'col' in kw:
    kw['c']=kw.pop['col']
  ret.append(pltfunc(x,y,marker=pch,cmap=cmap,**kw))
  if colbar and 'c' in kw:
    if add:
      ret.append(plt.colorbar(ret[-1]))
    else:
      ret.append(fig.colorbar(ret[-1]))
  if box and not add:
    tmp=tgt.xaxis.get_data_interval()
    tgt.xaxis.set_view_interval(tmp[0],tmp[1])
    tgt.xaxis.set_major_locator(mpl.ticker.AutoLocator())
    tgt.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
  if hline is not None:
    hline = [hline] if not isinstance(hline,list) else hline
    for l in hline:
      tgt.plot([x.min(),x.max()],[l,l],'r')
  if vline is not None:
    vline = [vline] if not isinstance(vline,list) else vline
    for l in vline:
      tgt.plot([l,l],[y.min(),y.max()],'r')
  if not add:
    #Use the fontsize parameter if it's set
    fs=dict()
    if 'fontsize' in kw:
      fs['fontsize']=kw['fontsize']
    tgt.set_xlabel(xlab,**fs)
    tgt.set_ylabel(ylab,**fs)
    tgt.set_title(title,**fs)
  return ret

def heatMap(x,y,z,xres=5000,yres=5000, bar=True, log=True, xlab='', ylab='',
  title='',fig=None,subplot=111,**kw):
  """Given a set of coordinates {(x,y,z)}, grids up x and y and converts
  the z values into a colour scaling."""
  if not fig:
    fig=plt.figure()
  ret=[fig]
  tgt=fig.add_subplot(subplot)
  xi=np.linspace(x.min(),x.max(),xres)
  yi=np.linspace(y.min(),y.max(),yres)
  if log:
    z=np.log10(z)
  zi=griddata((x,y),z,(xi[None,:],yi[:,None]),method='linear')
  ex=[xi.min(),xi.max(),yi.max(),yi.min()]
  ret.append(tgt.imshow(zi,extent=ex,**kw))
  tgt.set_xlabel(xlab)
  tgt.set_ylabel(ylab)
  tgt.set_title(title)
  if bar:
    ret.append(fig.colorbar(ret[-1]))
  return ret

def ratioScatter(x, y1, y2=None, **kw):
  """ Designed for plotting log ratios.  If y1 and y2 both given, the y
  axis becomes log2(|y1|/|y2|), otherwise y1 is assumed to be a valid log
  ratio of two values."""
  if y2 is not None:
      y=np.log2(np.abs(y1)/np.abs(y2))
  else:
      y=y1
  return plot(x,y, hline=0,**kw)

def relerrorScatter(x,y1,y2=None, **kw):
  """ Calculates the relative error and then passes it to plot"""
  if y2 is not None:
    y=100.*(np.abs(y2-y1)/np.abs(y1))
  else:
    y=y1
  return plot(x,y,**kw)
