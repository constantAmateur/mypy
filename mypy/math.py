import numpy as np
from scipy.interpolate import interp1d
from scipy.ndimage import map_coordinates

def gauss_kern(size, sizey=None):
  """ Returns a normalized 2D gauss kernel array for convolutions """
  size = int(size)
  if not sizey:
      sizey = size
  else:
      sizey = int(sizey)
  x, y = mgrid[-size:size+1, -sizey:sizey+1]
  g = exp(-(x**2/float(size)+y**2/float(sizey)))
  return g / g.sum()

def sround(x,figs=0):
  pw=np.floor(np.log10(np.abs(x)))
  return 10**pw*round(x*10**-pw)

def propspace(a,b,n=50):
  """Basically just logspace, but I wanted to be able to "see" what I was
  doing."""
  K=((b/a)**(1./(n-1.)))-1.
  out=np.zeros(n)
  for i in xrange(n):
      out[i]=a*((1+K)**i)
  return out

def polar2cartesian(r, t, grid, x, y, order=3):
  """Coordinate transform for converting a polar array to Cartesian
  coordinates.  r and t are the coordinates on the current polar grid, grid
  are the values at each location and x,y, are the points where we are to
  interpolate in cartesian space.  A spline controls the interpolation, where
  order specificies the spline order (0-5)."""

  X, Y = np.meshgrid(x, y)

  new_r = np.sqrt(X*X+Y*Y)
  new_t = np.arctan2(X, Y)

  ir = interp1d(r, np.arange(len(r)), bounds_error=False)
  it = interp1d(t, np.arange(len(t)))

  new_ir = ir(new_r.ravel())
  new_it = it(new_t.ravel())

  new_ir[new_r.ravel() > r.max()] = len(r)-1
  new_ir[new_r.ravel() < r.min()] = 0
  return map_coordinates(grid, np.array([new_ir, new_it]),
                          order=order).reshape(new_r.shape)

def W(q,h,ndim=3):
  """A SPH smoothing function.  Here q=|r_a-r_b|/h"""
  if isinstance(q,int) or isinstance(q,float):
    q=np.array([q])
  if ndim==1:
    sigma=2./3.
  elif ndim==2:
    sigma=10./(7.*np.pi)
  elif ndim==3:
    sigma=1./np.pi
  else:
    raise NameError('ndim must be between 1-3')
  out=q.copy()
  c1=(q>=0) & (q<1)
  c2=(q>=1) & (q<2)
  c3=(q>=2)
  out[c1] = (sigma/(h**ndim))*(1.-(1.5)*q[c1]**2+(.75)*q[c1]**3)
  out[c2] = (sigma/(h**ndim))*(.25*(2-q[c2])**3)
  out[c3] = 0
  return out

def dWdq(q,h,ndim=3):
  """The derivative of the SPH smoothing function above."""
  if isinstance(q,int) or isinstance(q,float):
    q=np.array([q])
  term1=(ndim/q)*W(q,h,ndim=ndim)
  sigma3=(1./np.pi)/h**3
  sigma2=(10./(7.*np.pi))/h**2
  sigma1=(2./3.)/h
  if ndim==1:
    sigma=sigma1
  elif ndim==2:
    sigma=sigma2
  elif ndim==3:
    sigma=sigma3
  else:
    raise NameError('ndim must be between 1-3')
  out=q.copy()
  c1=(q>=0) & (q<1)
  c2=(q>=1) & (q<2)
  c3=(q>=2)
  out[c1] = sigma*((-.75*(2.-q)**2.)+3.*(1.-q)**2.)
  out[c2] = sigma*(-.75*(2.-q)**2.)
  out[c3] = 0.
  return term1+out

def dWdrho(q,h,rho,ndim=3):
  """The derivative of the SPH smoothing function with respect to rho, evaluated at rho."""
  if isinstance(q,int) or isinstance(q,float):
    q=np.array([q])
  sigma3=(1./np.pi)/(rho*h**3)
  sigma2=(10./(7.*np.pi))/(rho*h**2)
  sigma1=(2./3.)/(rho*h)
  if ndim==1:
    sigma=sigma1
  elif ndim==2:
    sigma=sigma2
  elif ndim==3:
    sigma=sigma3
  else:
    raise NameError('ndim must be between 1-3')
  out=q.copy()
  c1=(q>=0) & (q<1)
  c2=(q>=1) & (q<2)
  c3=(q>=2)
  out[c1] = (.25*(2.-q)**3.)-(((3.*q)/(4.*ndim))*(2.-q)**2)-((1.-q)**3.)+(((3.*q)/(ndim))*(1-q)**2)
  out[c2] = (.25*(2.-q)**3.)-(((3.*q)/(4.*ndim))*(2.-q)**2)
  out[c3] = 0.
  out=out*sigma
  return out

def Epanechnikov(q,h,ndim=3):
  """The smoothing kernel that is meant to have certain "optimal" properties for estimating a pdf
  from a finite sampling."""
  if isinstance(q,int) or isinstance(q,float):
    q=np.array([q])
  if ndim==1:
    sigma=3./(4.)
  elif ndim==2:
    sigma=4./(2.*np.pi)
  elif ndim==3:
    sigma=15./(8.*np.pi)
  out=q.copy()
  c1=(q>=0) & (q<1)
  c2=(q>=1)
  out[c1] = 1-q*q
  out[c2] = 0.
  out=(out*sigma)/(h**ndim)
  return out



